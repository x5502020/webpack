const SectionCreator = (() => {
  const create = (type) => {
    const programSection = document.querySelector(".join-program");
    const titleElement = programSection.querySelector("h1");
    const buttonElement = programSection.querySelector("button");

    const programData = {
      standard: {
        title: "Join Our Program",
        buttonText: "Subscribe",
      },
      advanced: {
        title: "Join Our Advanced Program",
        buttonText: "Subscribe to Advanced Program",
      },
    };

    if (!programData[type]) {
      throw new Error(`Invalid program type: ${type}`);
    }

    const { title, buttonText } = programData[type];
    titleElement.textContent = title;
    buttonElement.textContent = buttonText;

    const remove = () => {
      programSection.remove();
    };

    return { section: programSection, remove };
  };

  return { create };
})();

export default SectionCreator;
